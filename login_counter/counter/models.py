from django.db import models

# Create your models here.
class UsersModel(models.Model):
    user = models.CharField(max_length=128)
    password = models.CharField(max_length=128)
    count = models.IntegerField(default=1)
    
    def __unicode__(self):
        return "%s, %s" % (self.user, self.count)

    @staticmethod
    def login(user, password):
        search_result = UsersModel.objects.filter(user=user, password=password)
        if(len(search_result.all()) != 1):
            return -1
        u = search_result[0]
        u.count += 1
        u.save()
        return u.count

    @staticmethod
    def add(user, password):
        if user == '' or len(user) > 128:
            return -3
        if len(UsersModel.objects.filter(user=user).all()) > 0:
            return -2
        if len(password) > 128:
            return -4
        for c in password:
            if ord(c) > 127:
                return -4
        u = UsersModel(user=user, password=password)
        u.save()
        return u.count

    @staticmethod
    def TESTAPI_resetFixture():
        UsersModel.objects.all().delete()
        return 1

