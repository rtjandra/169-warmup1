#!/usr/bin/python
# -*- coding: utf-8 -*-
from models import UsersModel
from django.test import TestCase
import unittest

class TestUsersModel(unittest.TestCase):
    def setUp(self):
        UsersModel.objects.all().delete()
        u = UsersModel(user='finn', password='jake')
        u.save()

    def test_loginInvalidUser(self):
        self.assertEqual(UsersModel.login('bmo', 'noire'), -1)

    def test_loginValidUser(self):
        self.assertEqual(UsersModel.login('finn', 'jake'), 2)

    def test_addValidUser(self):
        self.assertEqual(UsersModel.add('asdf', 'asdf'), 1)

    def test_addEmptyUser(self):
        self.assertEqual(UsersModel.add('', 'asdf'), -3)

    def test_addLargeUser(self):
        self.assertEqual(UsersModel.add('asdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdf', 'asdf'), -3)
    
    def test_addExistingUser(self):
        self.assertEqual(UsersModel.add('finn', 'asdf'), -2)
    
    def test_addBadPassword(self):
        self.assertEqual(UsersModel.add('asdf', 'à_à'), -4)

    def test_addLongPassWord(self):
        self.assertEqual(UsersModel.add('asdf','asdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdf'), -4)
    
    def test_addEmptyPassword(self):
        self.assertEqual(UsersModel.add('asdf', ''), 1)
    
    def test_addAndLoginCount(self):
        self.assertEqual(UsersModel.add('asdf', 'asdf'), 1)
        self.assertEqual(UsersModel.login('asdf', 'asdf'), 2)
    
    def test_resetFixture(self):
        UsersModel.TESTAPI_resetFixture()
        self.assertEqual(len(UsersModel.objects.all()), 0)

if __name__ == '__main__':
    unittest.main()
