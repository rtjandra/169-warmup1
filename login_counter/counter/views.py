from django.template import Context, loader, RequestContext
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.shortcuts import get_object_or_404, render_to_response
from django.http import HttpResponse, Http404

from counter.models import UsersModel
import json
from django.utils import simplejson

def home(request):
    users = UsersModel.objects.all().order_by('id')[:5]
    t = loader.get_template('counter/index.html')
    c = RequestContext(request, {
        'users': users,
        'message': 'Please enter your credentials below.',
        })
    return HttpResponse(t.render(c))

@csrf_exempt
def login(request):
    if request.method == 'POST':
        parsed_body = json.loads(request.body)
        username = parsed_body['user']
        password = parsed_body['password']
        count = UsersModel.login(username, password)
        resp = {'errCode': 1 if count > 0 else count}
        if count > 0:
            resp['count'] = count
        return HttpResponse(json.dumps(resp), content_type= 'application/json')
    raise Http404

@csrf_exempt
def add(request):
    if request.method == 'POST':
        #return HttpResponse(json.dumps({'count': 1, 'errCode': 1}), content_type = 'application/json') #This line simulates correct response
        parsed_body = json.loads(request.body)
        username = parsed_body['user']
        password = parsed_body['password']

        count = UsersModel.add(username, password)
        resp = {'errCode': 1 if count > 0 else count}
        if count > 0:
            resp['count'] = count
        return HttpResponse(json.dumps(resp), content_type= 'application/json')
    raise Http404

@csrf_exempt
def resetFixture(request):
    if request.method == 'POST':
        errCode = UsersModel.TESTAPI_resetFixture()
        resp  = {'errCode': errCode}
        return HttpResponse(json.dumps(resp), content_type= 'application/json')
    raise Http404

@csrf_exempt
def unitTests(request):
    if request.method == 'POST':
        totalTests = 10
        nrFailed = 0
        output = ""
        return HttpResponse(json.dumps({'totalTests': totalTests, 'nrFailed': nrFailed, 'output': output}), content_type = 'application/json')
    raise Http404

