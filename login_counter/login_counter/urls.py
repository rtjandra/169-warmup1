from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('counter.views',
    # Examples:
    url(r'^$', 'home', name='home'),
    url(r'^users/login/?$', 'login'),
    url(r'^users/add/?$', 'add'),
    url(r'^TESTAPI/resetFixture/?$', 'resetFixture'),
    url(r'^TESTAPI/unitTests/?$', 'unitTests'),
    # url(r'^login_counter/', include('login_counter.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
