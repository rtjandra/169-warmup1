/* Takes form information and a csrf token and posts login information
 * to the site and displays the corresponding response page
 *
 * @params
 * user - user's username
 * password - user's corresponding password
 *
 * */
function User_Login(form, token) {
    var postdata = {'csrfmiddlewaretoken': token,
        'user': form.user.value,
        'password': form.password.value
    };
    $.post('users/login/', JSON.stringify(postdata), function(data) {
        var parsed_data = JSON.parse(JSON.stringify(data));
        if ('count' in parsed_data) {
            var new_message = "Welcome " + postdata.user+ "</br>You have logged in " + parsed_data.count + " times.";
            $("#message").html(new_message);
            $("#input").addClass("hidden");
            $("#login").addClass("hidden");
            $("#logout").removeClass("hidden");
            $("#add").addClass("hidden");
        } else {
            $("#message").html(Common_ErrorHandler(parsed_data.errCode));
        }
    }, "json");
}

/*  Brings the user back to the main login page
 *
 * */
function User_Logout(form) {
    $("#input").removeClass("hidden");
    $("#login").removeClass("hidden");
    $("#logout").addClass("hidden");
    $("#add").removeClass("hidden");
    $("#message").html("Please enter your credentials below.");
}

/* Takes the form information and posts to the site to generate a new user
 *
 * @params
 *  user - user's username
 *  password - user's corresponding password
 */
function User_Add(form, token) {
    var postdata = {'csrfmiddlewaretoken': token,
        'user': form.user.value,
        'password': form.password.value
    };
    $.post('users/add/', JSON.stringify(postdata), function(data) {
        var parsed_data = JSON.parse(JSON.stringify(data));
        if ('count' in parsed_data) {
            var new_message = "Welcome " + postdata.user + "</br>You have logged in " + parsed_data.count + " times.";

            $("#message").html(new_message);
            $("#input").addClass("hidden");
            $("#login").addClass("hidden");
            $("#logout").removeClass("hidden");
            $("#add").addClass("hidden");
        } else {
            $("#message").html(Common_ErrorHandler(parsed_data.errCode));
        }
    }, "json");
}

/* Takes an error code and returns the corresponding error string.
 *
 * @params
 * errCode - error code
 *
 * */
function Common_ErrorHandler(errCode) {
    switch(errCode) {
        case -1:
            return "Invalid username and password combination. Please try again. ";
            break;
        case -2:
            return "This user name already exists. Please try again.";
            break;
        case -3:
            return "The user name should be non-empty and at most 128 characters long. Please try again.";
            break;
        case -4:
            return "The password should be at most 128 characters long. Please try again.";
            break;
    }
}
