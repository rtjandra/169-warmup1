#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Each file that starts with test... in this directory is scanned for subclasses of unittest.TestCase or testLib.RestTestCase
"""

import unittest
import os
import testLib
        
class TestAddUser(testLib.RestTestCase):
    """Test adding users"""
    def testAddEmpty(self):
        respData = self.makeRequest("/users/add", method="POST", data = { 'user' : '', 'password' : 'password'} )
        self.assertEqual(respData['errCode'], -3)

    def testAddLong(self):
        respData = self.makeRequest("/users/add", method="POST", data = { 'user' : 'asdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdf     asdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdf', 'password' : 'password'} )
        self.assertEqual(respData['errCode'], -3)

    def testAddExisting(self):
        respData = self.makeRequest("/users/add", method="POST", data = { 'user' : 'user1', 'password' : 'password'} )
        respData = self.makeRequest("/users/add", method="POST", data = { 'user' : 'user1', 'password' : 'password'} )
        self.assertEqual(respData['errCode'], -2)

    def testAddBadPassword(self):
        respData = self.makeRequest("/users/add", method="POST", data = { 'user' : 'user1', 'password' : 'Ã _Ã'} )
        self.assertEqual(respData['errCode'], -4)

    def testAddLong(self):
        respData = self.makeRequest("/users/add", method="POST", data = { 'password' : 'asdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdf     asdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdf', 'user' : 'password'} )
        self.assertEqual(respData['errCode'], -4)

    def testEmptyPassword(self):
        respData = self.makeRequest("/users/add", method="POST", data = {'user' : 'password', 'password': ''} )
        self.assertEqual(respData['errCode'], 1)

    
class TestLoginUser(testLib.RestTestCase):
    """test logging in"""
    def testLogin(self):
        respData = self.makeRequest("/users/add", method="POST", data = {'user' : 'password', 'password': ''} )
        respData = self.makeRequest("/users/login", method="POST", data = {'user' : 'password', 'password': ''} )
        self.assertEqual(respData['count'], 2)
        self.assertEqual(respData['errCode'], 1)

    def testLoginInvalid(self):
        respData = self.makeRequest("/users/login", method="POST", data = {'user' : 'password', 'password': ''} )
        self.assertEqual(respData['errCode'], -1)

    def testLoginMultiple(self):
        respData = self.makeRequest("/users/add", method="POST", data = {'user' : 'password', 'password': ''} )
        respData = self.makeRequest("/users/login", method="POST", data = {'user' : 'password', 'password': ''} )
        self.assertEqual(respData['count'], 2)
        self.assertEqual(respData['errCode'], 1)

        respData = self.makeRequest("/users/add", method="POST", data = {'user' : 'password2', 'password': ''} )
        respData = self.makeRequest("/users/login", method="POST", data = {'user' : 'password2', 'password': ''} )
        self.assertEqual(respData['count'], 2)
        self.assertEqual(respData['errCode'], 1)
